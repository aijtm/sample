package com.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.*;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

@Configuration
public class MongoConfig {

    //local 용
    @Bean
    public MongoTemplate mongoTemplate(MongoDbFactory mongoDbFactory, MongoMappingContext context) {
        MappingMongoConverter converter = new MappingMongoConverter(new DefaultDbRefResolver(mongoDbFactory), context);
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory, converter);
        return mongoTemplate;
    }

//    @Primary
//    @Bean(name = "primaryMongoTemplate")
//    public MongoTemplate primaryMongoTemplate() throws Exception {
//        return new MongoTemplate(primaryFactory());
//    }
//
//    @Bean(name = "secondaryMongoTemplate")
//    public MongoTemplate secondaryMongoTemplate() throws Exception {
//        return new MongoTemplate(secondaryFactory());
//    }

//    @Bean
//    @Primary
//    public MongoDbFactory primaryFactory() throws Exception {
//
//        MongoCredential mongoCredential = MongoCredential.createCredential(this.primaryUser, this.primaryDatabase, this.primaryPassword.toCharArray());
//        //MongoClient mongoClient = new MongoClient(new ServerAddress(this.secondaryUrl,  this.secondaryPort), Arrays.asList(mongoCredential));
//        return new SimpleMongoDbFactory(new MongoClient(new ServerAddress(this.primaryUrl,  this.primaryPort), Arrays.asList(mongoCredential)),
//                this.primaryDatabase);
//    }
//
//    @Bean
//    public MongoDbFactory secondaryFactory() throws Exception {
//
//        MongoCredential mongoCredential = MongoCredential.createCredential(this.secondaryUser, this.secondaryDatabase, this.secondaryPassword.toCharArray());
//        //MongoClient mongoClient = new MongoClient(new ServerAddress(this.secondaryUrl,  this.secondaryPort), Arrays.asList(mongoCredential));
//        return new SimpleMongoDbFactory(new MongoClient(new ServerAddress(this.secondaryUrl,  this.secondaryPort), Arrays.asList(mongoCredential)),
//                this.secondaryDatabase);
//    }

//    @Bean(name="mongoClient")
//    public MongoClient mongoClient() {
//        try {
//          MongoCredential mongoCredential = MongoCredential.createCredential(this.user, this.database, this.password.toCharArray());
//          MongoClient mongoClient = new MongoClient(new ServerAddress("localhost",  27107), Arrays.asList(mongoCredential));
//          return mongoClient;
//        } catch (Exception e){
//            return null;
//        }
//    }
}
