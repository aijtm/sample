package com.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

@Configuration
public class RootConfig {

    private static final Logger tlogger = LoggerFactory.getLogger("Thread.in.Executor");

    private static final AtomicLong atomicLong = new AtomicLong(1);

    @Value("${http.pool.connection.max}")
    private int maxTotal;

    @Value("${http.pool.connection.max-default-per-route}")
    private int maxDefaultPerRoute;

    @Bean(name = "schedule", destroyMethod = "shutdownNow", autowire = Autowire.BY_NAME)
    public ScheduledExecutorService schedule(){
        return java.util.concurrent.Executors.newScheduledThreadPool(60,
                r -> {
                    Thread t= new Thread(r);
                    t.setName( r.getClass().getName()+"-"+ atomicLong.getAndIncrement());
                    t.setDaemon(true);
                    t.setPriority(Thread.NORM_PRIORITY);
                    t.setUncaughtExceptionHandler((t1, e) -> {
                        tlogger.error(t1.getName(),e);
                        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    });
                    return t;
                }
        );
    }

    @Bean(name = "executor", destroyMethod = "shutdownNow", autowire = Autowire.BY_NAME)
    public ExecutorService executor(){
        return new ThreadPoolExecutor(
                80,
                80,
                0L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>(1000),
                r -> {
                    Thread t= new Thread(r);
                    t.setName( r.getClass().getName()+"-"+ atomicLong.getAndIncrement());
                    t.setDaemon(true);
                    t.setPriority(Thread.NORM_PRIORITY);
                    t.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                        @Override
                        public void uncaughtException(Thread t, Throwable e) {
                            tlogger.error(t.getName(),e);
                            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }
                    });
                    return t;
                },
                new ThreadPoolExecutor.CallerRunsPolicy()
        );
    }

    @Bean(name = "poolingHttpClientConnectionManager", destroyMethod = "shutdown", autowire = Autowire.BY_NAME)
    public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager(){
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        //maximum number of total open connections.
        connManager.setMaxTotal(this.maxTotal);
        //maximum number of concurrent connections per route, which is 2 by default.
        connManager.setDefaultMaxPerRoute(this.maxDefaultPerRoute);
        //total number of concurrent connections to a specific route, which is 2 by default.
        //connManager.setMaxPerRoute(new HttpRoute(new HttpHost("api.onstove.com", 80)),4);
        connManager.closeIdleConnections(0,TimeUnit.MILLISECONDS);
        return connManager;
    }

    @Bean(name = "closeableHttpClient", destroyMethod = "close", autowire = Autowire.BY_NAME)
    public CloseableHttpClient closeableHttpClient(){
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        //maximum number of total open connections.
        connManager.setMaxTotal(this.maxTotal);
        //maximum number of concurrent connections per route, which is 2 by default.
        connManager.setDefaultMaxPerRoute(this.maxDefaultPerRoute);
        //total number of concurrent connections to a specific route, which is 2 by default.
        //connManager.setMaxPerRoute(new HttpRoute(new HttpHost("api.onstove.com", 80)),4);
        connManager.closeIdleConnections(0,TimeUnit.MILLISECONDS);
        return createHttpClient(connManager, 10000);
    }

    @Bean
    public RestTemplate restTemplate(){
        //return new RestTemplate(getClientHttpRequestFactory(poolingHttpClientConnectionManager,5000));
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        connManager.setMaxTotal(this.maxTotal);
        connManager.setDefaultMaxPerRoute(this.maxDefaultPerRoute);
        connManager.closeIdleConnections(0,TimeUnit.MILLISECONDS);
        return new RestTemplate(new HttpComponentsClientHttpRequestFactory(createHttpClient(connManager,5000)));
    }

    @Bean
    public RestTemplate restTemplateForInternal(){
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        connManager.setMaxTotal(this.maxTotal);
        connManager.setDefaultMaxPerRoute(this.maxDefaultPerRoute);
        connManager.closeIdleConnections(0,TimeUnit.MILLISECONDS);

        List<ClientHttpRequestInterceptor> interceptorList = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory(createHttpClient(connManager, 5000)));
        restTemplate.setInterceptors(interceptorList);

        return restTemplate;
    }

    @Bean
    public RestTemplate middleTermRestTemplate(){
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        connManager.setMaxTotal(this.maxTotal);
        connManager.setDefaultMaxPerRoute(this.maxDefaultPerRoute);
        connManager.closeIdleConnections(0,TimeUnit.MILLISECONDS);
        return new RestTemplate(new HttpComponentsClientHttpRequestFactory(createHttpClient(connManager,300000)));
    }

    @Bean
    public RestTemplate longTermRestTemplate(){
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        connManager.setMaxTotal(this.maxTotal);
        connManager.setDefaultMaxPerRoute(this.maxDefaultPerRoute);
        connManager.closeIdleConnections(0,TimeUnit.MILLISECONDS);
        return new RestTemplate(new HttpComponentsClientHttpRequestFactory(createHttpClient(connManager,600000)));
    }

    @Bean
    public ObjectMapper objectMapper(){
        return new ObjectMapper()
                //.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
                .enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS
                        , DeserializationFeature.USE_LONG_FOR_INTS
                        , DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT
                        , DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT
                        , DeserializationFeature.READ_ENUMS_USING_TO_STRING
                        , DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
                .enable(SerializationFeature.WRITE_NULL_MAP_VALUES
                        , SerializationFeature.WRITE_ENUMS_USING_TO_STRING)
                .enable(JsonGenerator.Feature.IGNORE_UNKNOWN)
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
    }

    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }

    private static CloseableHttpClient createHttpClient(PoolingHttpClientConnectionManager connManager, int timeoutMilli){
        CloseableHttpClient client = HttpClientBuilder
                .create()
                .setUserAgent("GMTOOL-FRONT SERVER")
                .setConnectionManager(connManager)
                .setKeepAliveStrategy((httpResponse, httpContext) -> {
                    HeaderElementIterator it = new BasicHeaderElementIterator
                            (httpResponse.headerIterator(HTTP.CONN_KEEP_ALIVE));
                    while (it.hasNext()) {
                        HeaderElement he = it.nextElement();
                        String param = he.getName();
                        String value = he.getValue();
                        if (value != null && param.equalsIgnoreCase("timeout")) {
                            return Long.parseLong(value) * 1000;
                        }
                    }
                    return 0;
                })
                .setDefaultSocketConfig(SocketConfig.custom()
                        .setSoKeepAlive(false)
                        .setSoReuseAddress(true)
                        .setTcpNoDelay(true)
                        .setSoTimeout(timeoutMilli)
                        .setSoLinger(0).build())
                .setDefaultRequestConfig(RequestConfig.custom()
                        .setConnectTimeout(timeoutMilli)
                        .setConnectionRequestTimeout(timeoutMilli)
                        .setSocketTimeout(timeoutMilli)
                        .build())
                .build();
        return client;
    }
}
