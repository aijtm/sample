package com.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.sql.DataSource;
import java.io.IOException;

@Configuration
@MapperScan(
        annotationClass= Repository.class,
        basePackages="com",
        sqlSessionFactoryRef="sqlSessionFactoryBean")
@EnableTransactionManagement  // TransactionManagement 를 사용할 경우.
public class DatasourceConfig {

    private Environment env;

    public DatasourceConfig(Environment env){
        this.env = env;
    }

    @Bean(destroyMethod = "close", name = "boDS", autowire = Autowire.BY_NAME)
    @Primary
    public DataSource boDS() {
        //why hikaricp so fast?
        //Bytecode-level engineering – some extreme bytecode level engineering (including assembly level native coding) has been done
        //Micro-optimizations – although barely measurable, these optimizations combined boost the overall performance
        //Intelligent use of the Collections framework – the ArrayList<Statement> was replaced with a custom class FastList that eliminates range checking and performs removal scans from tail to head
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl( env.getProperty("jdbc.bo.url") );
        config.setUsername( env.getProperty("jdbc.bo.user") );
        config.setPassword( env.getProperty("jdbc.bo.password") );
        config.setDriverClassName(env.getProperty("jdbc.bo.driver"));
        config.setMaximumPoolSize(env.getProperty("jdbc.bo.pool", Integer.class));
        config.setMinimumIdle(env.getProperty("jdbc.bo.pool", Integer.class));
        config.setConnectionTimeout(env.getProperty("jdbc.bo.connectiontimeout", Integer.class));
        config.setConnectionInitSql(env.getProperty("jdbc.bo.initsql"));
        config.setTransactionIsolation("TRANSACTION_READ_UNCOMMITTED");
        //https://github.com/brettwooldridge/HikariCP/wiki/MySQL-Configuration
        config.addDataSourceProperty( "cachePrepStmts" , "true" );
        config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.addDataSourceProperty( "useServerPrepStmts" , "true" );
        config.addDataSourceProperty("verifyServerCertificate","false");
        //https://github.com/brettwooldridge/HikariCP
        config.addDataSourceProperty( "maxLifetime" , "10000" );
        DataSource dataSource = new HikariDataSource( config );
/*
        net.sf.log4jdbc.Log4jdbcProxyDataSource dd = new Log4jdbcProxyDataSource(ds);
        net.sf.log4jdbc.tools.Log4JdbcCustomFormatter fm = new Log4JdbcCustomFormatter();
        fm.setLoggingType(LoggingType.MULTI_LINE);
        fm.setSqlPrefix("\r\n ");
        dd.setLogFormatter(fm);
*/
        return dataSource;
    }

    @Bean(name = "sqlSessionFactoryBean")
    public SqlSessionFactoryBean getSqlSessionFactoryBean(DataSource boDS, ApplicationContext applicationContext) throws IOException {
        SqlSessionFactoryBean sessionBean = new SqlSessionFactoryBean();
        sessionBean.setDataSource(boDS);
        sessionBean.setConfigLocation(applicationContext.getResource("classpath:mybatis-config.xml"));
        sessionBean.setMapperLocations(applicationContext.getResources("classpath*:**/repositories/*Mapper.xml"));
        return sessionBean;
    }

    //  destroyMethod = "close" 적용시 Invocation of destroy method 'close' failed on bean with name 'sqlSession': java.lang.UnsupportedOperationException: Manual close is not allowed over a Spring managed SqlSession
    @Bean
    public SqlSessionTemplate sqlSession(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

    @Bean(name = "boTransaction", autowire = Autowire.BY_NAME)
    public PlatformTransactionManager transactionManager() {
        return new DataSourceTransactionManager(boDS());
    }
}
