package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync(proxyTargetClass = true)  //use CGLIB for proxying
@EnableCaching(proxyTargetClass = true) //use CGLIB for proxying
@PropertySource(value = {"classpath:config.properties"})
@PropertySource(value = {"classpath:jdbc.properties"}, name = "dbconfig")
public class Application extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }
    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = SpringApplication.run(Application.class, args);
        System.out.println("***********************************");
        for (String name: applicationContext.getBeanDefinitionNames()) {
            System.out.println("    "+name);
        }
        System.out.println("***********************************");
    }
}
