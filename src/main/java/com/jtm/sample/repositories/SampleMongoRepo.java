package com.jtm.sample.repositories;

import org.springframework.stereotype.Repository;
import java.util.List;

public interface SampleMongoRepo<T> {

    T save(T t);
    T findByName(String s);
    List<T> findByAllName(String s);
    List<T> findAll();
    T insert(T t);
    void deleteById(String s);
    //public <T > List<T> saveAll(Iterable<T> iterable);
    //boolean existsById(String s);
//    public void delete(T t);

//    @Override
//    public Iterable<MongoDomain> findAllById(Iterable<String> iterable) {
//        return null;
//    }

//
//    @Override
//    public void deleteAll(Iterable<? extends MongoDomain> iterable) {
//
//    }
//
//    @Override
//    public void deleteAll() {
//
//    }
//
//    @Override
//    public List<MongoDomain> findAll(Sort sort) {
//        return null;
//    }
//
//    @Override
//    public Page<MongoDomain> findAll(Pageable pageable) {
//        return null;
//    }
//
//    @Override
//    public <S extends MongoDomain> List<S> insert(Iterable<S> iterable) {
//        return null;
//    }
//
//    @Override
//    public <S extends MongoDomain> Optional<S> findOne(Example<S> example) {
//        return Optional.empty();
//    }
//
//    @Override
//    public <S extends MongoDomain> List<S> findAll(Example<S> example) {
//        return null;
//    }
//
//    @Override
//    public <S extends MongoDomain> List<S> findAll(Example<S> example, Sort sort) {
//        return null;
//    }
//
//    @Override
//    public <S extends MongoDomain> Page<S> findAll(Example<S> example, Pageable pageable) {
//        return null;
//    }
//
//    @Override
//    public <S extends MongoDomain> long count(Example<S> example) {
//        return 0;
//    }
//
//    @Override
//    public <S extends MongoDomain> boolean exists(Example<S> example) {
//        return false;
//    }
}
