package com.jtm.sample.repositories;

import com.jtm.sample.domain.MongoTemplete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public class SampleMongoRepoImpl implements SampleMongoRepo<MongoTemplete> {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<MongoTemplete> findAll() {
        return mongoTemplate.findAll(MongoTemplete.class);
    }

    @Override
    public MongoTemplete save(MongoTemplete mongoDomain) {
        mongoTemplate.save(mongoDomain);
        return mongoDomain;
    }

    @Override
    public MongoTemplete findByName(String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));
        return (MongoTemplete) mongoTemplate.findOne(query, MongoTemplete.class);
    }

    @Override
    public List<MongoTemplete> findByAllName(String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));
        return mongoTemplate.find(query, MongoTemplete.class);
    }

    @Override
    public MongoTemplete insert(MongoTemplete mongoDomain) {
        return null;
    }

    @Override
    public void deleteById(String s) {

    }

//
//    @Override
//    public <S extends MongoDomain> S save(S s) {
//        mongoTemplate.save(s);
//        return s;
//    }
//
//    @Override
//    public <S extends MongoDomain> List<S> saveAll(Iterable<S> iterable) {
//        return null;
//    }
//
//    @Override
//    public Optional<MongoDomain> findById(String s) {
//        Query query = new Query();
//        query.addCriteria(Criteria.where("name").is(s));
//        return mongoTemplate.findOne(query, MongoDomain.class);
//    }
}
