package com.jtm.sample.contoller;

import com.common.dto.ResponseDto;
import com.jtm.sample.dto.MongoTestRequest;
import com.jtm.sample.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping(value = "Test")
@Validated
public class SampleController {

    @Autowired
    SampleService sampleService;

    @RequestMapping(value = "mongo-findAll", method = RequestMethod.GET)
    public ResponseDto mongo (){
        return sampleService.findAll();
    }

    @RequestMapping(value = "mongo-findByAllName", method = RequestMethod.POST)
    public ResponseDto mongoFindByAllName(@Valid @RequestBody MongoTestRequest mongoTestRequest){
        return sampleService.findByAllName(mongoTestRequest.getName());
    }

    @RequestMapping(value = "mongo-findByName", method = RequestMethod.GET)
    public ResponseDto mongoFindByName(@Valid @NotBlank @RequestParam(value = "name", required = false) String name){
        return sampleService.findByName(name);
    }
}
