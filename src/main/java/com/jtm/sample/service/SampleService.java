package com.jtm.sample.service;

import com.common.dto.ResponseDto;
import com.jtm.sample.domain.MongoTemplete;
import com.jtm.sample.repositories.SampleMongoRepoImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SampleService {

    Logger logger = LoggerFactory.getLogger(SampleService.class);

//    @Autowired
//    SampleMongoRepoImpl sampleMongoRepoImpl;

    /* Select All */
    public ResponseDto findAll(){
        ResponseDto<MongoTemplete> responseDto = new ResponseDto<>();
        List<MongoTemplete>  mongoTempletes = new ArrayList<>();
        MongoTemplete mongoTemplete = new MongoTemplete();
        mongoTemplete.setDesc("TestDesc");
        mongoTemplete.setName("Test");
        mongoTempletes.add(mongoTemplete);
//        responseDto.setDatas(sampleMongoRepoImpl.findAll());
        responseDto.setTotalCount(mongoTempletes.size());
        return responseDto;
    }

    /* List Name */
    public ResponseDto findByAllName(String name){
        ResponseDto<MongoTemplete> responseDto = new ResponseDto<>();
//        responseDto.setDatas(sampleMongoRepoImpl.findByAllName(name));
        responseDto.setTotalCount(responseDto.getDatas().size());
        return responseDto;
    }

    /* One Name */
    public ResponseDto findByName(String name){
        ResponseDto<MongoTemplete> responseDto = new ResponseDto<>();
//        responseDto.setData(sampleMongoRepoImpl.findByName(name));
        responseDto.setTotalCount(1);
        return responseDto;
    }
}
