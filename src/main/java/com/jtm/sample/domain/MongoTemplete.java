package com.jtm.sample.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Basic;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Document(collection = "sample")
public class MongoTemplete implements Serializable {
    private static final long serialVersionUID = 7090276721431241044L;

    @Id
    @JsonIgnore
    private ObjectId id;
    private String name;
    private String desc;
}
