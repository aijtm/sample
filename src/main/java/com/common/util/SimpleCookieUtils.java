package com.common.util;

import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public enum SimpleCookieUtils {
    INSTANCE;
    /**
     * Cookie를 통한 Access-Token 얻기
     * @param request
     * @return
     */
    public String getAccessToken(HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        String PHD= "";
        String PPLD= "";
        String PSIGN= "";
        String access_token= "";
        if(cookies != null) {
            for (Cookie cookie : cookies) {
                if( cookie.getName().equals("PHD") ) { PHD = cookie.getValue(); }
                if( cookie.getName().equals("PPLD") ) { PPLD = cookie.getValue(); }
                if( cookie.getName().equals("PSIGN") ) { PSIGN = cookie.getValue(); }
            }
            access_token = PHD + "." + PPLD + "." + PSIGN;
        }

        return access_token;
    }

    public String getRefreshToken(HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        String ARFT= "";
        for (Cookie cookie : cookies) {
            if( cookie.getName().equals("PRFT") ) { ARFT = cookie.getValue(); }
        }
        return ARFT;
    }

    /**
     * 응답 받은 파라미터를 통한 Cookie 추가
     * @param object
     * @param response
     * @throws JSONException
     */
    public void paramSetCookie(JSONObject object, HttpServletResponse response, String domain) throws JSONException {
        StringUtil stringUtil = new StringUtil();
        String AHD= "";
        String APLD= "";
        String ASIGN= "";
        String access_token= "";
        String ARFT= "";

        if(stringUtil.isNotEmpty(object)){
            access_token = object.get("access_token").toString();
            String[] accessTokenArray = access_token.split("[.]");
            if(accessTokenArray.length > 0){
                AHD = accessTokenArray[0];
                APLD = accessTokenArray[1];
                ASIGN = accessTokenArray[2];
            }
            ARFT = object.get("refresh_token").toString();
            setCookie(response,"PHD",AHD,domain);
            setCookie(response,"PPLD",APLD,domain);
            setCookie(response,"PSIGN",ASIGN,domain);
            setCookie(response,"PRFT",ARFT,domain);
        }
    }

    /**
     * Cookie 추가
     * @param response
     * @param name
     * @param value
     * @param domain
     */
    public void setCookie(HttpServletResponse response, String name, String value, String domain){
        Cookie cookie = new Cookie(name, value);
        // 쿠키 유효기간을 설정한다. 초단위 : 60*60*24= 1일
        cookie.setMaxAge(-1);
        cookie.setDomain(domain);
        cookie.setPath("/");
        // 응답헤더에 쿠키를 추가한다.
        response.addCookie(cookie) ;
    }

    /**
     * Cookie 삭제
     * @param response
     * @param cookieName
     * @param domain
     */
    public void removeCookie(HttpServletResponse response, String cookieName, String domain) {
        Cookie cookie = new Cookie(cookieName, null) ;
        cookie.setDomain(domain);
        cookie.setPath("/");
        cookie.setMaxAge(0) ;
        response.addCookie(cookie);
    }
    
    /**
     * Cookie Key value 조회
     * @param request
     * @return
     */
    public String getCookieValue(HttpServletRequest request, String cookieName) {
		Cookie[] cookies = request.getCookies();
		String value = null;
		
        if(cookies!=null){
        	for(Cookie cookie : cookies){
        		if(cookieName.equals(cookie.getName())) value = cookie.getValue();
        	}
        }
		
		return value;
	}

}