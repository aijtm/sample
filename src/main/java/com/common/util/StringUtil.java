package com.common.util;


import com.google.common.base.Strings;
import org.json.JSONArray;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class StringUtil {

    public static boolean isEmpty(final Integer value) {
        if (value == null) {
            return true;
        }
        return value < 0;
    }

    public static boolean isEmpty(final Long value) {
        if (value == null) {
            return true;
        }
        return value < 0;
    }

    public static boolean isEmpty(final String value) {
        return Strings.isNullOrEmpty(value);
    }

    public static boolean isEmpty(Object value) {
        if (value == null) {
            return true;
        } else {
            if (value instanceof String) {
                return !(value.toString().length() > 0);
            } else if (value instanceof List) {
                return !(((List<Object>) value).size() > 0);
            } else if (value instanceof String[]) {
                return !(((String[]) value).length > 0);
            } else if (value instanceof HashMap) {
                return !(((HashMap) value).keySet().size() > 0);
            }/* else if (value instanceof JSONObject) {
                return !(((JSONObject) value).size() > 0);
            }*/ else if (value instanceof JSONArray) {
                return !(((JSONArray) value).length() > 0);
            } else {
                return false;
            }
        }
    }

    public static boolean isNotEmpty(Object value) {
        return !isEmpty(value);
    }

    /**
     * client IP 가져오기
     * @param request
     * @return String
     * @throws Exception
     */
    public static String getClientIP(HttpServletRequest request) {
        String clientIp = request.getHeader("X-Forwarded-For");

        if (clientIp == null || clientIp.length() == 0 || "unknown".equalsIgnoreCase(clientIp)) {
            clientIp = request.getHeader("Proxy-Client-IP");
        }

        if (clientIp == null || clientIp.length() == 0 || "unknown".equalsIgnoreCase(clientIp)) {
            clientIp = request.getHeader("WL-Proxy-Client-IP");
        }

        if (clientIp == null || clientIp.length() == 0 || "unknown".equalsIgnoreCase(clientIp)) {
            clientIp = request.getHeader("HTTP_CLIENT_IP");
        }

        if (clientIp == null || clientIp.length() == 0 || "unknown".equalsIgnoreCase(clientIp)) {
            clientIp = request.getHeader("HTTP_X_FORWARDED_FOR");
        }

        if (clientIp == null || clientIp.length() == 0 || "unknown".equalsIgnoreCase(clientIp)) {
            clientIp = request.getRemoteAddr();
        }

        if (clientIp.contains(",")) {
            return clientIp.split(",")[0].trim();
        }

        return clientIp;

    }

    /**
     * TZ_OFFSET 따른 시간 계산 (국가별 등록일 계산)
     * @param request
     * @param time
     * @return
     * @throws Exception
     */
    public static String replaceTimeZone(HttpServletRequest request, String time){

        //SimpleCookieUtils simpleCookieUtils = new SimpleCookieUtils();
        String newTime = "";

        try{

            if(isNotEmpty(SimpleCookieUtils.INSTANCE.getCookieValue(request, "TZ_OFFSET"))){
                int TZ_OFFSET = Integer.parseInt(SimpleCookieUtils.INSTANCE.getCookieValue(request, "TZ_OFFSET"));
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat transFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
                cal.setTime(transFormat.parse(time));
                cal.add(Calendar.MINUTE, TZ_OFFSET);
                newTime = transFormat.format(cal.getTime());
            }else{
                newTime =  time;
            }

        }catch(Exception e){
            newTime =  time;
        }

        return newTime;
    }

    public static boolean contains(List list, Object obj) {
        return list.contains(obj);
    }

    /**
     * 시간에 GDS의 TZ_OFFSET 적용 후 시간을 TimeInMillis로 반환
     * @param request
     * @param time
     * @param strFormat
     * @return
     */
    public static long strTimeZoneToTimeInMillis(HttpServletRequest request, String time, String strFormat){

        //SimpleCookieUtils simpleCookieUtils = new SimpleCookieUtils();
        long newTime = 0;

        try{

            int tz_offset = 0;
            if(isNotEmpty(SimpleCookieUtils.INSTANCE.getCookieValue(request, "TZ_OFFSET"))){
                tz_offset = Integer.parseInt(SimpleCookieUtils.INSTANCE.getCookieValue(request, "TZ_OFFSET"));
            }

            Calendar cal = Calendar.getInstance();
            SimpleDateFormat transFormat = new SimpleDateFormat(strFormat);
            cal.setTime(transFormat.parse(time));
            if(tz_offset != 0) {
                cal.add(Calendar.MINUTE, -tz_offset);
            }
            newTime = cal.getTimeInMillis();
        }catch(Exception e){
            //newTime =  time;
        }

        return newTime;
    }

    /**
     * 시작시간과 종료 시간 비교
     * @param request
     * @param strStartDate
     * @param strEndDate
     * @param dateFormat
     * @return
     * @throws ParseException
     */
    public static boolean compareDate(HttpServletRequest request, String strStartDate, String strEndDate, String dateFormat) throws ParseException {
        boolean result = false;

        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        Date startDate = format.parse(strStartDate);

        Date endDate = format.parse(strEndDate);
        if(startDate.getTime() >= endDate.getTime()) {
            result = false;
        } else {
            result = true;
        }

        return result;
    }

    /**
     * GDS의 TZ_OFFSET을 적용한 시간과 현재시간 비교
     * @param request
     * @param strDate
     * @param dateFormat
     * @return
     * @throws ParseException
     */
    public static boolean compareDateToNowDate(HttpServletRequest request, String strDate, String dateFormat) throws ParseException {
        boolean result = false;

        //SimpleCookieUtils simpleCookieUtils = new SimpleCookieUtils();
        int tz_offset = 0;
        if(isNotEmpty(SimpleCookieUtils.INSTANCE.getCookieValue(request, "TZ_OFFSET"))){
            tz_offset = Integer.parseInt(SimpleCookieUtils.INSTANCE.getCookieValue(request, "TZ_OFFSET"));
        }

        //GDS의 TZ_OFFSET 시간을 계산
        Calendar startCal = Calendar.getInstance();
        SimpleDateFormat transFormat = new SimpleDateFormat(dateFormat);
        startCal.setTime(transFormat.parse(strDate));
        if(tz_offset != 0) {
            startCal.add(Calendar.MINUTE, -tz_offset);
        }
        long newTime = startCal.getTimeInMillis();

        //현재 시간 계산
        Calendar endCal = Calendar.getInstance();
        endCal.setTime(new Date());
        long nowTime = endCal.getTimeInMillis();

        if(newTime <= nowTime) {
            result = false;
        } else {
            result = true;
        }

        return result;
    }
}
