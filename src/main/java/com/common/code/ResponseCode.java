package com.common.code;

public enum ResponseCode {
    // TODO ResponseDto에서 응답코드 모아오기.
    FAIL("500", "FAIL"),
    ERR_UNEXPECTED_EXCEPTION("10002", "Error : Unexpected Exception Error."),
    ERR_NULLPOINTER_EXCEPTION("10003", "Error : Nullpointer Exception Error."),
    ERR_BUSINESS_EXCEPTION("10010", "Error : Business Exception Error."),
    Success("0000", "Success");

    String responseCode;
    String responseMessage;

    ResponseCode(String responseCode, String responseMessage) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
}
