package com.common.dto;

import com.common.code.ResponseCode;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ResponseDto<T>{

    //@JsonProperty("response_code")
    private String responseCode;

    //@JsonProperty("response_message")
    private String responseMessage;

    //@JsonProperty("data")
    private T data;

    //@JsonProperty("datas")
    private List<T> datas;

    private int totalCount;
    public ResponseDto(){
        responseCode="000";
        responseMessage="Success";
    }

    public ResponseDto(ResponseCode responseCode){
        this.responseCode = responseCode.getResponseCode();
        this.responseMessage = responseCode.getResponseMessage();
    }

    public ResponseDto(ResponseCode responseCode, String message){
        this.responseCode = responseCode.getResponseCode();
        this.responseMessage = message;
    }
}
