package com.common.exception.handler;

import com.common.code.ResponseCode;
import com.common.dto.ResponseDto;
import com.common.exception.InvalidRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class BusinessExceptionHandler extends BaseExceptionHandler{

    @ExceptionHandler(InvalidRequestException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseDto exception(InvalidRequestException ex) {
        log(ex);
        ResponseDto result = new ResponseDto(ResponseCode.ERR_BUSINESS_EXCEPTION, ex.getMessage());
        return result;
    }
}
