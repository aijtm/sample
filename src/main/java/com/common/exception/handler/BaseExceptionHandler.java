package com.common.exception.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;

public class BaseExceptionHandler {

    private Logger error = LoggerFactory.getLogger("error");
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    protected void log(Exception ex){
        String message = getStackStringFromException(ex);
        error.error(message);
        logger.error(message);
    }

    private String getStackStringFromException(Exception ex){
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }
}
