package com.common.exception.handler;

import com.common.code.ResponseCode;
import com.common.dto.ResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class DeveloperBugExceptionHandler extends BaseExceptionHandler {

    /*  개발자 버그 */
    @ExceptionHandler(NullPointerException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseDto exception(NullPointerException ex) {
        log(ex);
        ResponseDto result = new ResponseDto(ResponseCode.ERR_NULLPOINTER_EXCEPTION, ex.getMessage());
        return result;
    }

    /*
     * 가급적 아래 exception hnalder를 타게 개발하지 말 것.
     * exception에 의도를 담아 정의 한 후 service에서 exception을 발생하여 handling 할 것.
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseDto exception(Exception ex) {
        log(ex);
        ResponseDto result = new ResponseDto(ResponseCode.FAIL, ex.getMessage());
        return result;
    }
}
