package com.common.exception;


import lombok.*;

@Data
public class InvalidRequestException extends RuntimeException{
    private String message;

    public InvalidRequestException(){}

    public InvalidRequestException (String message) {
        this.message = message;
    }
}
